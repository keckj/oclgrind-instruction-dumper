kernel void test_kernel(global int *in,  
        global int *out1,  global int *out2, global int *out4, 
        global int *out8, global int *out16) {

    int i = get_global_id(0);

    if (i == 0) {
        const int   vec1  = in[0];
        const int2  vec2  = vload2(0,  in);
        const int4  vec4  = vload4(0,  in);
        const int8  vec8  = vload8(0,  in);
        const int16 vec16 = vload16(0, in);
 
        const int   res1  = max(vec1,  11);
        const int2  res2  = max(vec2,  11);
        const int4  res4  = max(vec4,  11);
        const int8  res8  = max(vec8,  11);
        const int16 res16 = max(vec16, 11);

        out1[0] = res1;
        vstore2(res2,   0, out2);
        vstore4(res4,   0, out4);
        vstore8(res8,   0, out8);
        vstore16(res16, 0, out16);
    }
}
