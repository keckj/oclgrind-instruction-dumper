
#include "InstructionDumper.h"

#include <map>
#include <set>
#include <iostream>
#include <thread>
#include <memory>
#include <cstdlib>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <cxxabi.h>

namespace oclgrind {
  constexpr bool read_dbg    = false; /* debug dump all memory reads */
  constexpr bool write_dbg   = false; /* debug dump all memory stores */
  constexpr bool float_dbg   = false; /* debug dump floating point operations */
  constexpr bool integer_dbg = false; /* debug dump integer operations */
  constexpr bool unknown_dbg = false; /* debug dump unknown instructions */
  constexpr bool dump_source = false; /* debug dump the line generating the call */

  const std::set<std::string> vload_functions = {
    "vload2", "vload3", "vload4", "vload8", "vload16", 
    "vload_half", "vload_half2", "vload_half3", "vload_half4", "vload_half8", "vload_half16",
    "vloada_half2", "vloada_half3", "vloada_half4", "vloada_half8", "vloada_half16"
  };
  const std::set<std::string> vstore_functions = {
    "vstore2", "vstore3", "vstore4", "vstore8", "vstore16", 
    "vstore_half", "vstore_half2", "vstore_half3", "vstore_half4", "vstore_half8", "vstore_half16",
    "vstorea_half2", "vstorea_half3", "vstorea_half4", "vstorea_half8", "vstorea_half16"
  };
  const std::set<std::string> hide_functions = {
    "get_work_dim", 
    "get_global_size", 
    "get_global_id", 
    "get_local_size", 
    "get_enqueued_local_size", 
    "get_local_id", 
    "get_num_groups", 
    "get_group_id", 
    "get_global_offset", 
    "get_global_linear_id", 
    "get_local_linear_id", 
    "wait_group_events",
    "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9"
  };
  const std::map<int, std::set<std::string>> known_arithmetic_functions = {
    {1, {
        "llvm.sqrt", "llvm.powi", "llvm.sin", "llvm.cos", "llvm.pow", "llvm.exp", "llvm.exp2", "llvm.log", "llvm.log10",
        "llvm.log2", "llvm.fabs", "llvm.minnum", "llvm.maxnum", "llvm.minimum", "llvm.maximum", "llvm.copysign", "llvm.floor", 
        "llvm.ceil", "llvm.trunc", "llvm.rint", "llvm.nearbyint", "llvm.round", "llvm.lround", "llvm.llround", "llvm.lrint", "llvm.llrint",
        "acos", "acosh", "acospi", "asin", "asinh", "asinpi", "atan", "atan2", "atanh", "atanpi", 
        "atan2pi", "cbrt", "ceil", "copysign", "cos", "cosh", "cospi", "erfc", "erf", "exp", 
        "exp2", "exp10", "expm1", "fabs", "fdim", "floor", "fmax", "fmin", "fmod", 
        "fract", "frexp", "hypot", "ilogb", "ldexp", "lgamma", "lgamma_r", "log", "log2", "log10", 
        "log1p", "logb", "modf", "nan", "nextafter", "pow", "pown", "powr", "remainder", 
        "remquo", "rint", "rootn", "round", "rsqrt", "sin", "sincos", "sinh", "sinpi", "sqrt", 
        "tan", "tanh", "tanpi", "tgamma", "trunc", 
        "half_cos", "half_divide", "half_exp", "half_exp2", "half_exp10", 
        "half_log", "half_log2", "half_log10", "half_powr", "half_recip", 
        "half_rsqrt", "half_sin", "half_sqrt", "half_tan", 
        "native_cos", "native_divide", "native_exp", "native_exp2", "native_exp10", 
        "native_log", "native_log2", "native_log10", "native_powr", "native_recip", 
        "native_rsqrt", "native_sin", "native_sqrt", "native_tan",
        "degrees", "max", "min", "radians", "sign", "step"
        "abs", "add_sat", "clz", "ctz", 
        "max", "min", "mul_hi", "mul24", "rotate", "sub_sat", "upsample", "popcount", 
      }
    },
    {2, {
        "llvm.fmuladd", "llvm.fma", 
        "mad", "mad_sat", "mad_hi", "mad24", "fma",
        "clamp", "dot", "length", "fast_length", 
        "abs_diff",  "hadd"
      }
    },
    {3, {
        "mix", "cross", "distance", "normalize", "fast_distance", "fast_normalize", "rhadd"
      }
    },
  };
  const std::set<std::string> all_known_arithmetic_functions = computeUnion(known_arithmetic_functions);

  thread_local InstructionData
    InstructionDumper::m_data = InstructionData();

  
  void InstructionDumper::kernelBegin(const KernelInvocation *kernelInvocation) {
    m_kernel = kernelInvocation->getKernel();
    m_kernel_data.clear();
    const Size3 wgs = kernelInvocation->getNumGroups();
    const std::size_t work_group_size = wgs.x * wgs.y * wgs.z;
    if(m_onlyBenchFirstWorkGroup) {
      //std::cerr << "  >Oclgrind::InstructionDumper plugin will extrapolate data from first work group." << std::endl;
      m_showProgress = nullptr;
    }
    else {
      //std::cerr << "  >Oclgrind::InstructionDumper plugin will gather data from all work groups." << std::endl;
      m_showProgress = new boost::progress_display(work_group_size, std::cerr, 
          "  ", "  ", "  ");
    }
  }

  void InstructionDumper::workGroupBegin(const WorkGroup *workGroup) {
    m_data.clear();
    m_data.memoryOpCounts.emplace("load",  AddressSpaceValues<opcount_t>());
    m_data.memoryOpCounts.emplace("store", AddressSpaceValues<opcount_t>());
    m_data.memoryOpCounts.emplace("total", AddressSpaceValues<opcount_t>());
    m_data.memoryOpBytes.emplace("load",   AddressSpaceValues<std::size_t>());
    m_data.memoryOpBytes.emplace("store",  AddressSpaceValues<std::size_t>());
    m_data.memoryOpBytes.emplace("total",  AddressSpaceValues<std::size_t>());
  }

  void InstructionDumper::instructionExecuted(const WorkItem *workItem, 
    const llvm::Instruction *instruction, const TypedValue& result) {
    const llvm::DebugLoc& location = instruction->getDebugLoc();
    const opcode_t opcode = instruction->getOpcode();
    const std::size_t work_group_index = workItem->getWorkGroup()->getGroupIndex();
    const bool dbg_cond = (workItem->getLocalID()==Size3(1,0,0) && work_group_index==0);
    if (m_onlyBenchFirstWorkGroup && (work_group_index != 0)) {
      return;
    }

    std::stringstream ss, ss2;
    ss << llvm_opcodes.at(opcode) << "::";
    if (location) {
      const int line=location.getLine(), col=location.getCol();
      ss << line << "::" << col << "::";
      if (dump_source) {
        ss2 << m_kernel->getProgram()->getSourceLine(line) << std::endl;
        for (int i=0; i<col-1; i++) { ss2 << ' '; }
        ss2 << '^' << std::endl;
      }
    }
    ss << llvmObjectToString(*instruction);
    const std::string sinstruction = ss.str();
    const std::string src_location= ss2.str();

    std::map<opcode_t, opcount_t>&             instructionCounts     = m_data.instructionCounts;
    std::map<opname_t, AddressSpaceValues<opcount_t>>&   memoryOpCounts      = m_data.memoryOpCounts;
    std::map<opname_t, AddressSpaceValues<std::size_t>>& memoryOpBytes       = m_data.memoryOpBytes;
    std::map<opname_t, opcount_t>&             funcCalls         = m_data.funcCalls;
    std::map<opcount_t, opcount_t>&            integerOperations     = m_data.integerOperations;
    std::map<opcount_t, opcount_t>&            floatingPointOperations = m_data.floatingPointOperations;
    if (instructionCounts.find(opcode) != instructionCounts.end()) {
      instructionCounts[opcode] += 1;
    }
    else {
      instructionCounts.emplace(opcode, 1);
    }
    
    if (opcode == llvm::Instruction::Load || opcode == llvm::Instruction::Store) {
      const bool load = (opcode == llvm::Instruction::Load);
      const llvm::PointerType& type = static_cast<llvm::PointerType&>(*(instruction->getOperand(load?0:1)->getType()));
      unsigned int addrSpace = type.getPointerAddressSpace();
      const std::string& saddrSpace = addressSpaces.at(addrSpace);
      std::size_t bytes = getTypeSize(type.getPointerElementType());
      if (load) {
        memoryOpCounts["load"][addrSpace] += 1;
        memoryOpBytes["load"][addrSpace]  += bytes;
        if (read_dbg && dbg_cond && saddrSpace=="global") { std::cout << bytes << "B load from " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
      }
      else {
        memoryOpCounts["store"][addrSpace] += 1;
        memoryOpBytes["store"][addrSpace]  += bytes;
        if (write_dbg && dbg_cond && saddrSpace=="global") { std::cout << bytes << "B store to " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
      }
      memoryOpCounts["total"][addrSpace] += 1;
      memoryOpBytes["total"][addrSpace]  += bytes;
    }
    else if (instruction->isBinaryOp()) {
      /* unary ops are also considered as binary ops in the LLVM IR */
      llvm::Type *resultType = instruction->getType();
      llvm::Type *scalarType;
      unsigned int numElements, bitWidth;
      if (resultType->isVectorTy()) {
        scalarType  = resultType->getVectorElementType();
        numElements = resultType->getVectorNumElements();
      }
      else {
        scalarType = resultType;
        numElements = 1;
      }
      bitWidth = scalarType->getPrimitiveSizeInBits();
      const std::string soperation = std::to_string(bitWidth)+" bit binary op on ("+std::to_string(numElements)+" x "+llvmObjectToString(*scalarType)+')' + " elements from " + sinstruction;
      if (scalarType->isIntegerTy()) {
        if (integerOperations.find(bitWidth) != integerOperations.end()) {
          integerOperations[bitWidth] += numElements;
        }
        else {
          integerOperations.emplace(bitWidth, numElements);
        }
        if (integer_dbg && dbg_cond) { std::cout << soperation << std::endl << src_location; }
      }
      else if (scalarType->isFloatingPointTy()) {
        if (floatingPointOperations.find(bitWidth) != floatingPointOperations.end()) {
          floatingPointOperations[bitWidth] += numElements;
        }
        else {
          floatingPointOperations.emplace(bitWidth, numElements);
        }
        if (float_dbg && dbg_cond) { std::cout << soperation << std::endl << src_location; }
      }
      else {
        if (unknown_dbg && dbg_cond) { std::cout << soperation << std::endl << src_location; }
      }
    }
    else if (opcode == llvm::Instruction::Call) {
      const llvm::CallInst *callInst = (const llvm::CallInst*)instruction;
      const llvm::Function *func = callInst->getCalledFunction();
      if (func) {
        const std::string mangled_name = func->getName();
        std::string name;
        {
          int status = -1;
          char* res = abi::__cxa_demangle(mangled_name.c_str(), NULL, NULL, &status);
          if (status==0) {
            name = std::string(res);
            std::free(res);
          }
          else {
            name = mangled_name;
          }
        }
        if (this->handle_fcall(name, *workItem, *callInst, *func, sinstruction, src_location, dbg_cond)) {
          if (funcCalls.find(name) != funcCalls.end()) {
            funcCalls[name] += 1;
          }
          else {
            funcCalls.emplace(name, 1);
          }
        }
      }
    }
  }
    
  bool InstructionDumper::handle_fcall(const std::string& fname, 
      const WorkItem& workItem, const llvm::CallInst& call, const llvm::Function& func,
      const std::string& sinstruction, const std::string& src_location, bool dbg_cond) {
    std::map<opname_t, AddressSpaceValues<std::size_t>>& memoryOpBytes       = m_data.memoryOpBytes;
    std::map<opcount_t, opcount_t>&            integerOperations     = m_data.integerOperations;
    std::map<opcount_t, opcount_t>&            floatingPointOperations = m_data.floatingPointOperations;
    if (std::any_of(vload_functions.begin(), vload_functions.end(), 
          [&](const std::string& fprefix)->bool{ return (fname == fprefix) || (fname.rfind(fprefix+'(', 0)==0); })) {
      std::size_t bytes = getTypeSize(func.getReturnType());
      unsigned int addrSpace = call.getArgOperand(1)->getType()->getPointerAddressSpace();
      memoryOpBytes["load"][addrSpace]  += bytes;
      memoryOpBytes["total"][addrSpace] += bytes;
      const std::string& saddrSpace = addressSpaces.at(addrSpace);
      if (dbg_cond && read_dbg && saddrSpace=="global") { std::cout << bytes << "B vector load from " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
    }
    else if (std::any_of(vstore_functions.begin(), vstore_functions.end(), 
          [&](const std::string& fprefix)->bool{ return (fname == fprefix) || (fname.rfind(fprefix+'(', 0)==0); })) {
      std::size_t bytes = getTypeSize(call.getArgOperand(0)->getType());
      unsigned int addrSpace = call.getArgOperand(2)->getType()->getPointerAddressSpace();
      memoryOpBytes["store"][addrSpace] += bytes;
      memoryOpBytes["total"][addrSpace] += bytes;
      const std::string& saddrSpace = addressSpaces.at(addrSpace);
      if (dbg_cond && write_dbg && saddrSpace=="global") { std::cout << bytes << "B vector store to " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
    }
    else if ((fname.rfind("async_work_group_copy", 0) == 0) ||
         (fname.rfind("async_work_group_strided_copy", 0) == 0)) {
      if (workItem.getLocalID()==Size3(0,0,0)) {
        const std::size_t numElements = workItem.getOperand(call.getArgOperand(2)).getUInt();
        {
          const llvm::PointerType* dst = static_cast<llvm::PointerType*>(call.getArgOperand(0)->getType());
          unsigned int addrSpace = dst->getPointerAddressSpace();
          std::size_t bytes = getTypeSize(dst->getPointerElementType());
          memoryOpBytes["store"][addrSpace] += numElements*bytes;
          memoryOpBytes["total"][addrSpace] += numElements*bytes;
          const std::string& saddrSpace = addressSpaces.at(addrSpace);
          if (dbg_cond && write_dbg && saddrSpace=="global") { std::cout << numElements*bytes << "B async store to " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
        }
        {
          const llvm::PointerType* src = static_cast<llvm::PointerType*>(call.getArgOperand(1)->getType());
          unsigned int addrSpace = src->getPointerAddressSpace();
          std::size_t bytes = getTypeSize(src->getPointerElementType());
          memoryOpBytes["load"][addrSpace]  += numElements*bytes;
          memoryOpBytes["total"][addrSpace] += numElements*bytes;
          const std::string& saddrSpace = addressSpaces.at(addrSpace);
          if (dbg_cond && read_dbg && saddrSpace=="global") { std::cout << numElements*bytes << "B async read from " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
        }
      }
    }
    else if ((fname.rfind("llvm.memcpy",  0) == 0) || 
         (fname.rfind("llvm.memmove", 0) == 0)) {
      const std::size_t nbytes = workItem.getOperand(call.getArgOperand(2)).getUInt();
      {
        const llvm::PointerType* dst = static_cast<llvm::PointerType*>(call.getArgOperand(0)->getType());
        unsigned int addrSpace = dst->getPointerAddressSpace();
        memoryOpBytes["store"][addrSpace] += nbytes;
        memoryOpBytes["total"][addrSpace] += nbytes;
        const std::string& saddrSpace = addressSpaces.at(addrSpace);
        if (dbg_cond && write_dbg && saddrSpace=="global") { std::cout << nbytes << "B memcpy to " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
      }
      {
        const llvm::PointerType* src = static_cast<llvm::PointerType*>(call.getArgOperand(1)->getType());
        unsigned int addrSpace = src->getPointerAddressSpace();
        memoryOpBytes["load"][addrSpace]  += nbytes;
        memoryOpBytes["total"][addrSpace] += nbytes;
        const std::string& saddrSpace = addressSpaces.at(addrSpace);
        if (dbg_cond && write_dbg && saddrSpace=="global") { std::cout << nbytes << "B memcpy from " << saddrSpace << " memory from " << sinstruction << std::endl << src_location; }
      }
    }
    else if (std::any_of(all_known_arithmetic_functions.begin(), all_known_arithmetic_functions.end(), 
          [&](const std::string& fprefix)->bool{ return fname.rfind(fprefix, 0)==0; })) {
      
      int operationCount = 0;
      for (auto const& [k, v] : known_arithmetic_functions) {
        if (std::any_of(v.begin(), v.end(), 
            [&](const std::string& fprefix)->bool{ return fname.rfind(fprefix, 0)==0; })) {
          operationCount = k;
          break;
        }
      }
      assert(operationCount>=0);

      llvm::Type *resultType = func.getReturnType();
      llvm::Type *scalarType;
      unsigned int numElements, bitWidth;
      if (resultType->isVectorTy()) {
        scalarType  = resultType->getVectorElementType();
        numElements = resultType->getVectorNumElements();
      }
      else {
        scalarType = resultType;
        numElements = 1;
      }
      bitWidth = scalarType->getPrimitiveSizeInBits();
      const std::string soperation = std::to_string(bitWidth)+" bit: function '" + fname + "' (~" + std::to_string(operationCount) + "op/call) on ("+std::to_string(numElements)+" x "+llvmObjectToString(*scalarType)+')' + " elements from " + sinstruction;
      if (scalarType->isIntegerTy()) {
        if (integerOperations.find(bitWidth) != integerOperations.end()) {
          integerOperations[bitWidth] += operationCount*numElements;
        }
        else {
          integerOperations.emplace(bitWidth, operationCount*numElements);
        }
        if (integer_dbg && dbg_cond) { std::cout << soperation << std::endl << src_location; }
      }
      else if (scalarType->isFloatingPointTy()) {
        if (floatingPointOperations.find(bitWidth) != floatingPointOperations.end()) {
          floatingPointOperations[bitWidth] += operationCount*numElements;
        }
        else {
          floatingPointOperations.emplace(bitWidth, operationCount*numElements);
        }
        if (float_dbg && dbg_cond) { std::cout << soperation << std::endl << src_location; }
      }
      else {
        if (unknown_dbg && dbg_cond) { std::cout << soperation << std::endl << src_location; }
      }
    }
    else if (std::any_of(hide_functions.begin(), hide_functions.end(), 
          [&](const std::string& fprefix)->bool{ return fname.rfind(fprefix, 0)==0; })) {}
    else {
      return true;
    }
    return false;
  }
  
  void InstructionDumper::workGroupComplete(const WorkGroup *workGroup) {
    const std::size_t work_group_index = workGroup->getGroupIndex();
    if (m_onlyBenchFirstWorkGroup) {
      m_data.workGroupExecuted += (work_group_index == 0);
    }
    else {
      m_data.workGroupExecuted += 1;
    }
    {
      std::lock_guard<std::mutex> lock(m_mtx);
      m_kernel_data += m_data;
      if (!m_onlyBenchFirstWorkGroup) {
        ++(*m_showProgress);
      }
    }
  }
      
  void InstructionDumper::kernelEnd(const KernelInvocation *kernelInvocation) {
    m_kernel = nullptr;
    const Size3 wgs = kernelInvocation->getNumGroups();
    const std::size_t work_group_size = wgs.x * wgs.y * wgs.z;
    namespace pt = boost::property_tree;
    pt::ptree opcodes, op_categories, op_counts,
      memory_op_counts, memory_op_bytes, func_calls,
      iops, flops, work_group;
    for (auto const& [k, v] : m_kernel_data.instructionCounts) {
      opcodes.put(std::to_string(k), llvm_opcodes.at(k));
      op_counts.put(llvm_opcodes.at(k), v);
    }
    for (auto const& [k, v] : llvm_op_categories) {
      pt::ptree cell, cellpair;
      cell.put_value(v.first);
      cellpair.push_back({"",cell});
      cell.put_value(v.second);
      cellpair.push_back({"",cell});
      op_categories.add_child(k, cellpair);
    }
    for (auto const& [opname, counts] : m_kernel_data.memoryOpCounts) {
      pt::ptree opdata;
      for (auto const& [addrSpace, addrSpaceName] : addressSpaces) {
        opdata.put(addrSpaceName, counts[addrSpace]);
      }
      memory_op_counts.add_child(opname, opdata);
    }
    for (auto const& [opname, bytes] : m_kernel_data.memoryOpBytes) {
      pt::ptree opdata;
      for (auto const& [addrSpace, addrSpaceName] : addressSpaces) {
        opdata.put(addrSpaceName, bytes[addrSpace]);
      }
      memory_op_bytes.add_child(opname, opdata);
    }
    for (auto const& [k, v] : m_kernel_data.funcCalls) {
      func_calls.put(k, v);
    }
    for (auto const& [k, v] : m_kernel_data.integerOperations) {
      iops.put(std::to_string(k), v);
    }
    for (auto const& [k, v] : m_kernel_data.floatingPointOperations) {
      flops.put(std::to_string(k), v);
    }
    work_group.put("executed", m_kernel_data.workGroupExecuted);
    work_group.put("total", work_group_size);

    pt::ptree root;
    //root.add_child("opcodes", opcodes);
    root.add_child("op_counts", op_counts);
    //root.add_child("op_categories", op_categories);
    //root.add_child("memory_op_counts", memory_op_counts);
    root.add_child("memory_op_bytes", memory_op_bytes);
    root.add_child("function_calls", func_calls);
    root.add_child("integer_ops", iops);
    root.add_child("floating_point_ops", flops);
    root.add_child("work_group", work_group);
    pt::write_json(std::cout, root);
    
    delete m_showProgress;
  }
  
  extern "C" {
    void initializePlugins(Context *context) {
      context->registerPlugin(new InstructionDumper(context));
    }
  }
}
