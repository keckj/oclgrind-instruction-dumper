#include "oclgrind/Context.h"
#include "oclgrind/Plugin.h"
#include "oclgrind/Program.h"
#include "oclgrind/WorkItem.h"
#include "oclgrind/WorkGroup.h"

#include "core/common.h"
#include "core/Kernel.h"
#include "core/KernelInvocation.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/Support/raw_ostream.h"

#include <boost/progress.hpp>

#include <set>
#include <map>
#include <mutex>
#include <cstdlib>
#include <string>
#include <cassert>

namespace oclgrind {
    using opcode_t  = int;
    using opname_t  = std::string;
    using opcount_t = unsigned int;

    /* llvm intermediate representation:  opcodes -> instruction name */
    const std::map<opcode_t, opname_t> llvm_opcodes = {
        #define HANDLE_INST(N, OPC, CLASS) {N, #OPC},
        #include "llvm/IR/Instruction.def"
    };

    /* llvm intermediate representation: instruction category name -> (first_opcode, last_opcode) */
    const std::map<opname_t, std::pair<opcode_t, opcode_t>> llvm_op_categories = {
        #define FIRST_TERM_INST(N) {"term", {N,
        #define LAST_TERM_INST(N) N}},
        #define FIRST_BINARY_INST(N) {"binary", {N,
        #define LAST_BINARY_INST(N) N}},
        #define FIRST_MEMORY_INST(N) {"memory", {N,
        #define LAST_MEMORY_INST(N) N}},
        #define FIRST_CAST_INST(N) {"cast", {N,
        #define LAST_CAST_INST(N) N}},
        #define FIRST_FUNCLETPAD_INST(N) {"funcletpad", {N,
        #define LAST_FUNCLETPAD_INST(N) N}},
        #define FIRST_OTHER_INST(N) {"other", {N,
        #define LAST_OTHER_INST(N) N}},
        #include "llvm/IR/Instruction.def"
    };
        
    const std::map<unsigned int, std::string> addressSpaces = {
            {AddrSpacePrivate,  "private"},
            {AddrSpaceGlobal,   "global"},
            {AddrSpaceConstant, "constant"},
            {AddrSpaceLocal,    "local"},
            {4,                 "unknown"}
    };

    template <typename T>
    struct AddressSpaceValues {
        AddressSpaceValues(): m_private(0), m_local(0), m_global(0), m_constant(0), m_unknown(0) {}
        T m_private, m_local, m_global, m_constant, m_unknown;
        T& operator [](unsigned int addrSpace) {
            if      (addrSpace == AddrSpacePrivate)  { return m_private;  }
            else if (addrSpace == AddrSpaceGlobal)   { return m_global;   }
            else if (addrSpace == AddrSpaceLocal)    { return m_local;    }
            else if (addrSpace == AddrSpaceConstant) { return m_constant; }
            else                                     { return m_unknown;  }
        }
        const T& operator [](unsigned int addrSpace) const {
            if      (addrSpace == AddrSpacePrivate)  { return m_private;  }
            else if (addrSpace == AddrSpaceGlobal)   { return m_global;   }
            else if (addrSpace == AddrSpaceLocal)    { return m_local;    }
            else if (addrSpace == AddrSpaceConstant) { return m_constant; }
            else                                     { return m_unknown;  }
        }
        AddressSpaceValues<T>& operator+=(const AddressSpaceValues<T>& rhs) {
            for (const auto& addrSpace : addressSpaces) {
                this->operator[](addrSpace.first) += rhs[addrSpace.first];
            }
            return *this;
        }
    };

    class InstructionData {
        public:
            InstructionData(): instructionCounts(), funcCalls(),
                memoryOpCounts(), memoryOpBytes(), workGroupExecuted(0){};
            std::map<opcode_t, opcount_t> instructionCounts;
            std::map<opname_t, opcount_t> funcCalls;
            std::map<opname_t, AddressSpaceValues<opcount_t>>   memoryOpCounts;
            std::map<opname_t, AddressSpaceValues<std::size_t>> memoryOpBytes;
            std::map<opcount_t, opcount_t> integerOperations;
            std::map<opcount_t, opcount_t> floatingPointOperations;
            std::size_t workGroupExecuted;
            void clear() { 
                instructionCounts.clear(); 
                funcCalls.clear(); 
                memoryOpCounts.clear();
                memoryOpBytes.clear(); 
                integerOperations.clear();
                floatingPointOperations.clear();
                workGroupExecuted = 0;
            }
            InstructionData& operator+=(const InstructionData& rhs) {
                for( const auto& [k,v] : rhs.instructionCounts ) {
                    if (this->instructionCounts.find(k) != this->instructionCounts.end()) {
                        this->instructionCounts[k] += v;
                    }
                    else {
                        this->instructionCounts.emplace(k, v);
                    }
                }
                for( const auto& [k,v] : rhs.memoryOpCounts ) {
                    if (this->memoryOpCounts.find(k) != this->memoryOpCounts.end()) {
                        this->memoryOpCounts[k] += v;
                    }
                    else {
                        this->memoryOpCounts.emplace(k, v);
                    }
                }
                for( const auto& [k,v] : rhs.memoryOpBytes ) {
                    if (this->memoryOpBytes.find(k) != this->memoryOpBytes.end()) {
                        this->memoryOpBytes[k] += v;
                    }
                    else {
                        this->memoryOpBytes.emplace(k, v);
                    }
                }
                for( const auto& [k,v] : rhs.funcCalls ) {
                    if (this->funcCalls.find(k) != this->funcCalls.end()) {
                        this->funcCalls[k] += v;
                    }
                    else {
                        this->funcCalls.emplace(k, v);
                    }
                }
                for( const auto& [k,v] : rhs.integerOperations ) {
                    if (this->integerOperations.find(k) != this->integerOperations.end()) {
                        this->integerOperations[k] += v;
                    }
                    else {
                        this->integerOperations.emplace(k, v);
                    }
                }
                for( const auto& [k,v] : rhs.floatingPointOperations ) {
                    if (this->floatingPointOperations.find(k) != this->floatingPointOperations.end()) {
                        this->floatingPointOperations[k] += v;
                    }
                    else {
                        this->floatingPointOperations.emplace(k, v);
                    }
                }
                this->workGroupExecuted += rhs.workGroupExecuted;
                return *this;
            }
    };

    class InstructionDumper : public Plugin {
        public:

            InstructionDumper(const Context *context) : 
                Plugin(context), m_kernel_data(), m_onlyBenchFirstWorkGroup(false), 
                m_showProgress(nullptr), m_kernel(nullptr) {
                m_onlyBenchFirstWorkGroup = (std::getenv("OCLGRIND_ONLY_FIRST_WORKGROUP")!=nullptr);
            };

            virtual void instructionExecuted(
                    const WorkItem *workItem,
                    const llvm::Instruction *instruction,
                    const TypedValue& result) override;
            virtual void kernelBegin(const KernelInvocation *kernelInvocation) override;
            virtual void kernelEnd(const KernelInvocation *kernelInvocation) override;
            virtual void workGroupBegin(const WorkGroup *workGroup) override;
            virtual void workGroupComplete(const WorkGroup *workGroup) override;

        private:
            static thread_local InstructionData m_data;

            std::mutex m_mtx;
            InstructionData m_kernel_data;
            bool m_onlyBenchFirstWorkGroup;
            boost::progress_display* m_showProgress;
            const Kernel* m_kernel;
        
        private:
            bool handle_fcall(const std::string& fname, 
                    const WorkItem& workItem, const llvm::CallInst& call, const llvm::Function& func,
                    const std::string& sintruction, const std::string& src_location, bool dbg_cond);
        
            template <typename T>
            std::string llvmObjectToString(const T& obj) {
                std::string type_str;
                llvm::raw_string_ostream rso(type_str);
                obj.print(rso);
                rso.str();
                return type_str;
        }
    };
    template <typename K, typename V>
    constexpr std::set<V> computeUnion(const std::map<K, std::set<V>>& sets) {
      std::set<V> result;
      for (auto const& [k, v] : sets) {
          result.insert(v.begin(), v.end());
      }
      return result;
    }
}
