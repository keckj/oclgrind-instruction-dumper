CXX      ?= g++
LLVM_DIR ?= /opt/llvm/llvm-6.0.1
CXXFLAGS += -I${LLVM_DIR}/include -I.
CXXFLAGS += -std=c++17 -fPIC -O3 -g
CXXFLAGS += -pedantic -Wall -Wextra -Wno-unused -Wno-missing-field-initializers
LDFLAGS  += -shared -L${LLVM_DIR}/lib -Wl,-rpath=${LLVM_DIR}/lib -loclgrind

all: libOclgrindInstructionDumper.so
	
Plugin.o:
	git submodule init
	git submodule update
	find oclgrind -type d -name 'core' -exec ln -sf {} \;
	${CXX} ${CXXFLAGS} -c core/Plugin.cpp -o Plugin.o

libOclgrindInstructionDumper.so: src/InstructionDumper.cpp src/InstructionDumper.h Plugin.o
	${CXX} ${CXXFLAGS} Plugin.o $< ${LDFLAGS} -o $@

test: test/test.cl test/test.sim
	oclgrind-kernel --plugins $(shell pwd)/libOclgrindInstructionDumper.so test/test.sim 2>/dev/null
clean:
	${RM} core libOclgrindInstructionDumper.so Plugin.o
.PHONY: test clean
