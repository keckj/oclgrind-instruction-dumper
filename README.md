# oclgrind instruction dumper

Collect statistics about executed OpenCL instructions during an Oclgrind simulation.

Build
=====
Build with: `CXX=g++ LLVM_DIR=/opt/llvm/llvm-6.0.1 make` or simply `make`

Clean with: `make clean`

Test with: `make test`

Basic usage
===========
`oclgrind-kernel --plugins $(pwd)/libOclgrindInstructionDumper.so test/test.sim 2>/dev/null`
